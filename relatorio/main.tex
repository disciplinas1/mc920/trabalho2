%-----------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%-----------------------------------------------

\documentclass[a4paper, 11pt]{article}

\usepackage{geometry}
\geometry{
    a4paper,
    top    = 1.5cm,
    bottom = 1.5cm,
    left   = 2.0cm,
    right  = 2.0cm
}

% \usepackage[utf8]{inputenc}
% \usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{mathtools}
\usepackage[portuguese]{babel}
\usepackage[version=3]{mhchem}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{indentfirst}
\usepackage{titlesec}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{etoolbox}
\usepackage{xcolor}
\usepackage{pdfpages}
\usepackage{listings}
\usepackage{subcaption}

%--- REFS --------------------------------

\usepackage{hyperref}
\usepackage[nameinlink,capitalize]{cleveref}

\hypersetup{
    pdftitle={Trabalho 1 -- RA220063},
    pdfauthor={Leonardo de Sousa Rodrigues},
    colorlinks=true,
    linkcolor=blue,
    citecolor=red
}

%--- CODE LISTING ----------------------

\usepackage{listings}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}


% Enlarge spacing in align environment
\addtolength{\jot}{8pt}


%--- EQREF ----------------------------------------

%equations ref brackets
\makeatletter
\let\oldtheequation\theequation
\renewcommand\tagform@[1]{\maketag@@@{\ignorespaces#1\unskip\@@italiccorr}}
\renewcommand\theequation{(\oldtheequation)}
\makeatother


% align elements in matrix
\makeatletter
\renewcommand*\env@matrix[1][r]{\hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols #1}}
\makeatother


%--- HEADER ----------------------------

\usepackage{fancyhdr}
\pagestyle{fancy}
\setlength{\headheight}{24pt}
\fancyhf{}
\lhead{MC920 -- Introdução ao Processamento de Imagem Digital}
\rhead{\today}

%----------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------

\begin{document}

\begin{center}
    {\LARGE Trabalho 2 - Quantização de Imagens com Difusão de Erro} \\[10pt]
    {\large Leonardo de Sousa Rodrigues -- RA: 220063}
\end{center}

\section{Introdução}

    Esse trabalho teve como objetivo a aplicação da operação de quantização de uma imagem RGB, ou seja, a transformação de uma imagem com diferentes níveis de vermelho, verde e azul para uma nova imagem com níveis binários dessas cores.

    Para isso, foram utilizadas algumas técnicas de difusão de erro: ao realizar a transformação, parte da informação é perdida e existe um erro entre o píxel original e o criado. Esse erro deve ser difundido entre outros píxels para melhorar a semelhança entre a imagem produzida e a original.

\section{Execução \label{sec:exec}}

    O arquivo \texttt{quantiza.py}, ao ser executado, recebe 3 argumentos posicionais e um opcional, conforme a tabela~\ref{tab:args}.

    \begin{table}[H]
    \def\arraystretch{1.5}\tabcolsep=15pt
    \centering
    \begin{tabular}{l p{0.6\textwidth}}
        \toprule
        \multicolumn{1}{c}{\textbf{Argumento}} & \multicolumn{1}{c}{\textbf{Descrição}} \\
        \midrule
        \multicolumn{1}{c}{IMAGEM} & Arquivo de imagem no formato \texttt{.png} a ser processado. \\
        \multicolumn{1}{c}{SAIDA} & Nome do arquivo em que a imagem de saída será salva. \\
        \multicolumn{1}{c}{DIST} & Distribuição de erro a ser utilizada. \\
        \texttt{-z, --zigzag} & Flag opcional para desabilitar o percurso alternado de linhas ao processar a imagem de entrada. \\
        \bottomrule
    \end{tabular}
    \caption{Tabela com os argumentos do programa \texttt{quantiza.py} e suas descrições.}
    \label{tab:args}
    \end{table}

    Um exemplo de execução do programa:

    \vspace{5pt}
    \hspace{\parindent} \texttt{python quantiza.py entrada/baboon.png saida/baboon.png a}
    \vspace{5pt}

    Nesse caso, a imagem \texttt{entrada/baoon.png} será quantizada com a difusão de erro (a): Floyd e Steinberg, com o percurso alternado de linhas e o resultado será salvo em \texttt{saida/baboon.png}.


\section{Implementação}

    \subsection{Entrada e Saída}

        As operações de leitura e escrita das imagens são feitas com o \texttt{matplotlib.pyplot}, que trata as imagens como arrays de 3 dimensões, sendo a terceira de tamanho 3, para as 3 cores. Os valores são passados para \texttt{floats} entre 0 e 1.

    \subsection{Distribuições de Erro}

        As distribuições especificadas no trabalho estão no arquivo \texttt{distrib.py}. Nele, está definida a classe \texttt{Dist()}, que contém uma matriz, cujos valores são os pesos para a alocação do erro, e também uma tupla que representa a posição da matriz que deve ser sobreposta ao píxel em análise. Por padrão, essa posição é no centro da primeira linha, como em todos os casos nesse trabalho, mas isso pode ser alterado.
        
        Cada máscara é adicionada ao dicionário \texttt{distrib}, sendo que a chave é a letra (\texttt{'a'}, \texttt{'b'}, \texttt{'c'}, ...) correspondente e o valor é o objeto da classe \texttt{Dist()}.

    \subsection{Quantização e Difusão de Erro}
    
        O processo de quantização e difusão de erro foram implementados de forma não vetorizada, com laços de repetição para percorrer tanto a imagem como a máscara de distribuição.

        Em um primeiro momento, é feita uma cópia da imagem original, que será alterada de acordo com os erros dos píxels anteriores. Cada valor $x$ de cor de cada píxel da imagem é analisada, determinando o valor para a imagem de saída (0 se $x < 0.5$ e 1 caso contrário). O erro desse valor é dado pela diferença entre o valor real e o valor quantizado. A máscara de distribuição é então percorrida juntamente com a vizinhança do píxel, adicionando o erro ponderado pelo peso da máscara aos píxels subsequentes.

        Como nesse trabalho o interesse era no resultado geral do processamento, sem a necessidade de detalhes finos, os erros que seriam passados a píxels fora da borda foram ignorados, sem grandes consequências à imagem.

    \subsection{Formas de Varredura}

        Por padrão (sem a flag \texttt{--zigzag}), a imagem original é percorrida de forma alternada: em linhas pares para a direita e em linhas ímpares para a esquerda. No caso da aplicação da direita para a esquerda, a máscara é percorrida da mesma forma, mas os erros ponderados são aplicados também de forma invertida na imagem, para que sejam sempre somados a píxels que ainda não foram analisados.

        No caso em que a flag \texttt{--zigzag} é utilizada, a imagem é percorrida sempre da esquerda para a direita, sem a necessidade de atenção especial.

\section{Imagens de Entrada}

    \begin{figure}[H]
    \centering
    \begin{subfigure}[b]{.30\linewidth}
        \includegraphics[width=\textwidth]{imagens/entrada/baboon.png}
        \caption{\texttt{baboon.png}}
    \end{subfigure}
    \begin{subfigure}[b]{.30\linewidth}
        \includegraphics[width=\textwidth]{imagens/entrada/peppers.png}
        \caption{\texttt{peppers.png}}
    \end{subfigure}
    \begin{subfigure}[b]{.30\linewidth}
        \includegraphics[width=\textwidth]{imagens/entrada/monalisa.png}
        \caption{\texttt{monalisa.png}}
    \end{subfigure} \\[10pt]
    \begin{subfigure}[b]{.50\linewidth}
        \includegraphics[width=\textwidth]{imagens/entrada/watch.png}
        \caption{\texttt{watch.png}}
    \end{subfigure}
    \caption{Imagens utilizadas com entrada no trabalho.}
    \label{fig:entrada}
    \end{figure}

    
\section{Resultados}

    Cada imagem resultante dos processamentos é quantizada, e possui píxels que variam entre $2^3 = 8$ opções, conforme o esquema da figura~\ref{fig:pixels}.

    \begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{imagens/pixels.jpg}
    \caption{Esquema de cores RGB binário e suas 8 combinações possíveis.}
    \label{fig:pixels}
    \end{figure}

    \subsection{Quantização sem Difusão de Erros}

        Fazendo a quantização da imagem sem aplicar nenhuma técnica para considerar os erros, o resultado é muito longe da imagem original, conforme mostra a figura~\ref{fig:vazio}.

        \begin{figure}[H]
        \centering
        \begin{subfigure}[b]{.30\linewidth}
            \includegraphics[width=\textwidth]{imagens/vazio/vaziobaboon.png}
            \caption{\texttt{baboon.png}}
        \end{subfigure}
        \begin{subfigure}[b]{.30\linewidth}
            \includegraphics[width=\textwidth]{imagens/vazio/vaziopeppers.png}
            \caption{\texttt{peppers.png}}
        \end{subfigure}
        \begin{subfigure}[b]{.30\linewidth}
            \includegraphics[width=\textwidth]{imagens/vazio/vaziomonalisa.png}
            \caption{\texttt{monalisa.png}}
        \end{subfigure} \\[10pt]
        \begin{subfigure}[b]{.50\linewidth}
            \includegraphics[width=\textwidth]{imagens/vazio/vaziowatch.png}
            \caption{\texttt{watch.png}}
        \end{subfigure}
        \caption{Imagens quantizadas sem difusão de erro.}
        \label{fig:vazio}
        \end{figure}

    \newpage
    \subsection{Formas de Varredura}

        Os resultados dos processamentos utilizando as duas formas de varredura foram bem parecidos, sendo possível notar diferenças mínimas ao ampliar muito a imagem, como mostra a figura~\ref{fig:varredura}.

        \begin{figure}[H]
        \centering
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/nozigzag/ababoon.png}
            \caption{Varredura unidirecional.}
        \end{subfigure}
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/zigzag/ababoon.png}
            \caption{Varredura alternada.}
        \end{subfigure} \\[10pt]
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/nozigzag/apeppers.png}
            \caption{Varredura unidirecional.}
        \end{subfigure}
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/zigzag/apeppers.png}
            \caption{Varredura alternada.}
        \end{subfigure} \\[10pt]
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/nozigzag/amonalisa.png}
            \caption{Varredura unidirecional.}
        \end{subfigure}
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/zigzag/amonalisa.png}
            \caption{Varredura alternada.}
        \end{subfigure} \\[10pt]
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/nozigzag/awatch.png}
            \caption{Varredura unidirecional.}
        \end{subfigure}
        \begin{subfigure}[b]{.3\linewidth}
            \includegraphics[width=\textwidth]{imagens/varredura/zigzag/awatch.png}
            \caption{Varredura alternada.}
        \end{subfigure} \\[10pt]
        \caption{Comparação entre as formas de varredura para as imagens com a distribuição de erros de Floyd e Steinberg.}
        \label{fig:varredura}
        \end{figure}

    \newpage
    \subsection{Diferentes Distribuições de Erro}

        Para ser possível a comparação das diferentes distribuições, serão dados exemplos com a mesma imagem para cada uma delas na figura~\ref{fig:distrib}, utilizando a varredura alternada.

        \begin{figure}[H]
        \centering
        \begin{subfigure}[b]{.40\linewidth}
            \includegraphics[width=\textwidth]{imagens/distrib/apeppers.png}
            \caption{Floyd e Steinberg}
        \end{subfigure}
        \begin{subfigure}[b]{.40\linewidth}
            \includegraphics[width=\textwidth]{imagens/distrib/bpeppers.png}
            \caption{Stevenson e Arce}
        \end{subfigure}\\[10pt]
        \begin{subfigure}[b]{.40\linewidth}
            \includegraphics[width=\textwidth]{imagens/distrib/cpeppers.png}
            \caption{Burkes}
        \end{subfigure}
        \begin{subfigure}[b]{.40\linewidth}
            \includegraphics[width=\textwidth]{imagens/distrib/dpeppers.png}
            \caption{Sierra}
        \end{subfigure}\\[10pt]
        \begin{subfigure}[b]{.40\linewidth}
            \includegraphics[width=\textwidth]{imagens/distrib/epeppers.png}
            \caption{Stucki}
        \end{subfigure}
        \begin{subfigure}[b]{.40\linewidth}
            \includegraphics[width=\textwidth]{imagens/distrib/fpeppers.png}
            \caption{Jarvis, Judice e Ninke}
        \end{subfigure}\\[10pt]
        \caption{Imagem \texttt{peppers.png} quantizada com as diferentes distribuições de erro.}
        \label{fig:distrib}
        \end{figure}

\section{Conclusão}

    Pela visualização da figura~\ref{fig:distrib}, fica claro que todas as distribuições de erro têm um resultado muito semelhante quando a figura não é observada com uma ampliação muito alta. Em todos os casos foi produzida uma imagem muito semelhante com a original, ou seja, o algoritmo cumpriu seu papel de forma satisfatória.

    É possível obervar, no entanto, algumas diferenças. A principal delas está na distribuição de Stevenson e Arce, que tem um resultado mais grosseiro que as demais, tanto em regiões de baixas frequências, que fica claro no interior dos objetos, como nas bordas.

\end{document}
