"""
Tipos de distribuições de erro aplicadas
na forma matricial.
"""
import numpy as np

class Dist():
    def __init__(self, matriz, pos=None):
        soma = np.sum(matriz)
        self.matriz = matriz / soma if soma != 0 else matriz
        self.pos = (0, len(matriz[0]) // 2) if pos is None else pos

distrib = {

    'vazio': Dist(np.asarray([[0]])),

    'a': Dist(np.asarray([
        [0, 0, 7],
        [3, 5, 1]
    ])),

    'b': Dist(np.asarray([
        [ 0,  0,  0,  0,  0, 32,  0],
        [12,  0, 26,  0, 30,  0, 16],
        [ 0, 12,  0, 26,  0, 12,  0],
        [ 5,  0, 12,  0,  12,  0, 5]
    ])),

    'c': Dist(np.asarray([
        [0, 0, 0, 8, 4],
        [2, 4, 8, 4, 2]
    ])),

    'd': Dist(np.asarray([
        [0, 0, 0, 5, 3],
        [2, 4, 5, 4, 2],
        [0, 2, 3, 2, 0]
    ])),

    'e': Dist(np.asarray([
        [0, 0, 0, 8, 4],
        [2, 4, 8, 4, 2],
        [1, 2, 4, 2, 1]
    ])),

    'f': Dist(np.asarray([
        [0, 0, 0, 7, 5],
        [3, 5, 7, 5, 3],
        [1, 3, 5, 3, 1]
    ]))

}
