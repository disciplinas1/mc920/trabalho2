"""
Módulo para quantização de imagens coloridas
utilizando a técnica de difusão de erro com
diferentes modelos de máscara.
"""
from sys import version_info

if version_info.major < 3 or version_info.minor < 5:
    erro = 'Esse programa suporta versões de Python à partir da 3.5'
    raise RuntimeError(erro)

import numpy as np
import argparse
from matplotlib import pyplot as plt
from copy import deepcopy
from distrib import distrib


def le_imagem(arquivo: str):
    """
    Retorna uma imagem colorida com valores do
    tipo float referente ao arquivo.
    """
    return plt.imread(arquivo).astype(float)


def quantiza(imagem, dist, zigzag=True):
    """
    Faz a quantização da imagem com a distribuição
    de erro de acordo com dist. Zigzag indica se a
    imagem deve ser percorrida de forma alternada a
    cada linha.
    """
    original = deepcopy(imagem)
    nova = np.zeros(original.shape)
    A, B, C = original.shape

    # Constantes para alinhar o píxel com a distribuição.
    SH_LIN = dist.pos[0]
    SH_COL = dist.pos[1]

    # Caso em que não há zigzag, iteração sempre igual.
    if not zigzag:
        colunas = range(B)
        direcao = 1

    # Iterações (i, j, k) sobre a imagem original.
    for i in range(A):

        # Caso com zigzag definido pela paridade da linha.
        if zigzag:
            if i % 2 == 0:
                colunas = range(B) 
                direcao = 1
            else:
                colunas = range(B - 1, -1, -1)
                direcao = -1           
        
        for j in colunas:
            for k in range(C):
                real = original[i, j, k]

                if real < 0.5:
                    nova[i, j, k] = 0
                else:
                    nova[i, j, k] = 1

                erro = real - nova[i, j, k]

                # Iterações (lin, col) sobre a matriz de distribuição de erro.
                for lin in range(dist.matriz.shape[0]):
                    for col in range(dist.matriz.shape[1]):
                        try:
                            original[i + lin - SH_LIN, j + direcao * (col - SH_COL), k] += erro * dist.matriz[lin, col]
                        except IndexError:
                            pass

    return nova


def main():
    """
    Lê os argumentos utilizados e realiza a quantização.
    Salva a imagem de saída de acordo com o nome especificado.
    """
    # Criação do ArgumentParser para lidar com argumentos da chamada e definição dos argumentos.
    parser = argparse.ArgumentParser(description='Quantiza imagens com difusão de erro.')

    parser.add_argument('IMAGEM', action='store', nargs=1, type=str,
        help='arquivo da imagem que será processada'
    )

    parser.add_argument('SAIDA', action='store', nargs=1, type=str,
        help='nome do arquivo de saída'
    )

    parser.add_argument('DIST', action='store', nargs=1, type=str,
        help='distribuição de erro a ser utilizada'
    )

    parser.add_argument('-z', '--zigzag', action='store_false', default=True,
        help='desliga o zigzag durante o percurso pela imagem'
    )


    # Lê os argumentos utilizados.
    if version_info.minor < 7:
        args = parser.parse_args()
    else:
        args = parser.parse_intermixed_args()

    print(args)


    # Lê a imagem e checa por existência do arquivo.
    entrada = le_imagem(args.IMAGEM[0])
    if entrada is None:
        raise FileNotFoundError


    # Processa a imagem.
    saida = quantiza(entrada, distrib[args.DIST[0].lower()], zigzag=args.zigzag)

    # Salva a imagem processada.
    plt.imsave(args.SAIDA[0], saida)


if __name__ == '__main__':
    main()
